<?php

$env = getenv();

$validateKeys = [
    "APP_ENV"           => "dev",
    "APP_NAME"          => "DEVELOPMENT",
    "APP_SECRET"        => "a88353db526afff45112165933d9fb76",
    "TRUSTED_PROXIES"   => "127.0.0.1,REMOTE_ADDR",
    "TRUSTED_HOSTS"     => "^{$_SERVER['SERVER_NAME']}",
    "DATABASE_URL"      => "mysql://www-data:www-password@mysql:3306/sylius?serverVersion=5.7",
];

foreach ($validateKeys as $key => $default) {
    if (!isset($env[$key])) {
        $env[$key] = $default;
    }
}

return $env;
